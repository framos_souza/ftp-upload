package br.com.ceva.it.integration.operations.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * A classe <code>IntegrationServiceApplication</code> possui a inicialização
 * do microservice integrationService Spring Boot.
 *
 * @author CEVA [19 de out de 2017]
 *
 */

import br.com.ceva.it.saga.integration.operations.config.SagaResponseConfig;

@SpringBootApplication
@Import({SagaResponseConfig.class})
public class UploadServiceApplication{

	public static void main(final String[] args) {
		SpringApplication.run(UploadServiceApplication.class, args);
	}

}
 