package br.com.ceva.it.saga.integration.operations.config;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import br.com.ceva.it.saga.integration.operations.gateway.FileWriterGateway;

@Configuration
@EnableIntegration
@PropertySource("classpath:application.properties")
@ImportResource("META-INF/spring/integration/FtpOutbound-context.xml")
public class SagaResponseConfig {

	@Autowired Environment environment; 	
 
	@Bean
    public MessageChannel sagaResponseChannel() {
        return new DirectChannel();
    }
	 
    @Bean
    @InboundChannelAdapter(value = "sagaResponseChannel", poller = @Poller(fixedDelay = "10000"))
    public MessageSource<File> fileReadingMessageSource() {
        FileReadingMessageSource sourceReader= new FileReadingMessageSource();
        sourceReader.setDirectory(new File(this.environment.getProperty("saga.integration.response.local.input.directory")));
        sourceReader.setFilter(new SimplePatternFileListFilter(this.environment.getProperty("saga.integration.filter")));
        return sourceReader;
    }
 
    @Bean(name = "sagaResponseMessageHandler")
    @ServiceActivator(inputChannel = "sagaResponseChannel")
    public MessageHandler handler() {
        return new MessageHandler() {
            
            @Autowired
        	private FileWriterGateway gateway;

			public void handleMessage(Message<?> message) throws MessagingException {
                
				File file = (File) message.getPayload();
		        this.gateway.write(file.getName(), file);
		        
		        file.delete();
            }
        };
    }

}
