package br.com.ceva.it.saga.integration.operations.gateway;

import java.io.File;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

public interface FileWriterGateway {

	public void write(@Header("fileName") String fileName, @Payload File message);
}
